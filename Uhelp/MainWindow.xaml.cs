﻿using System;
using System.Windows;
using System.Net;
using System.IO;
using System.Windows.Controls;

namespace Uhelp
{
    public partial class MainWindow : Window
    {
        private MyProcess myprocess;
        private string preCmdArgs = "";
        private string curCmdArgs = "";

        public MainWindow()
        {
            InitializeComponent();
            initExe();

            //argsTextBox.Text += string.Format("--playlist-items {0} ", epsText.Text);

            myprocess = new MyProcess(this);
            supportLink.RequestNavigate += myprocess.openLinkHandler;
        }

        // ===================
        // TextBox Components
        // ===================
        private void epsText_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (epsText.Text.Length > 0)
            {
                curCmdArgs = string.Format("--playlist-items {0} ", epsText.Text);
                Console.WriteLine(curCmdArgs);
            }
            else
            {
                curCmdArgs = "";
            }
        }


        private void epsText_LostFocus(object sender, RoutedEventArgs e)
        {

            // ===== epsText 相關操作 =====
            if ((preCmdArgs.Length == 0) && (curCmdArgs.Length != 0))
            {
                // 初始狀態，preCmdArgs 為空，curCmdArgs 不為空 => 直接添加
                addArgs(curCmdArgs, true);
                preCmdArgs = curCmdArgs;

            }
            else if( (preCmdArgs.Length != 0 ) && (curCmdArgs.Length == 0) )
            {
                // preCmdArgs 有賦值過，但 curCmdArgs 為空 => 代表取消使用 eps
                addArgs(preCmdArgs, false);
            }
            else if ( (preCmdArgs.Length != 0) && (curCmdArgs.Length != 0))
            {
                // preCmdArgs 和 curCmdArgs 都不為空，當兩個不相等的時候才添加
                if (preCmdArgs != curCmdArgs)
                {
                    addArgs(preCmdArgs, false);
                    addArgs(curCmdArgs, true);
                    preCmdArgs = curCmdArgs;
                }
            }
        }


        // ===================
        // CheckBox Components
        // ===================

        // 加入單影片參數
        private void singleVideoCheckBox_Click(object sender, RoutedEventArgs e)
        {
            string args = string.Format("--no-playlist ");
            
            if (curCmdArgs.Length > 0)
            {
                addArgs(curCmdArgs, false);
                preCmdArgs = "";
            }

            if (singleVideoCheckBox.IsChecked == true)
            {
                // 下載單一影片
                disableDownloadAllRelate();
                // 強制列表網址只下載單一影片
                addArgs("--no-playlist ", true);
            } 
            else
            {
                // 下載多個影片
                downloadAllCheckBox.IsEnabled = true;
                epsText.IsEnabled = true;
                addArgs("--no-playlist ", false);
            }
        }

        // 加入下載列表參數
        private void downloadAllCheckBox_Click(object sender, RoutedEventArgs e)
        {
            string args = string.Format("--yes-playlist ");

            if (curCmdArgs.Length > 0)
            {
                addArgs(curCmdArgs, false);
                preCmdArgs = "";
            }

            if (downloadAllCheckBox.IsChecked == true)
            {
                // 下載列表
                disableSingleVideoRelate();
                addArgs(args, true);
            }
            else
            {
                // 不下載列表
                singleVideoCheckBox.IsEnabled = true;
                epsText.IsEnabled = true;
                epsText.Text = "";

                addArgs(args, false);
            }
        }

        // 加入只下載音頻參數
        private void audioOnlyCheckBox_Click(object sender, RoutedEventArgs e)
        {
            string args = string.Format("-x --audio-format mp3 ");

            if (audioOnlyCheckBox.IsChecked == true)
            {
                addArgs(args, true);
            }
            else
            {
                addArgs(args, false);
            }
        }

        // ===================
        // buttons Components
        // ===================

        private async void goBtn_Click(object sender, RoutedEventArgs e)
        {
            await myprocess.download(argsTextBox.Text);
        }

        // ===================
        // help functions
        // ===================

        // download exe
        private void initExe()
        {
            Console.WriteLine("init exe");

            if (!File.Exists(@"youtubedl.exe"))
            {
                WebClient wc = new WebClient();
                wc.DownloadFile("https://yt-dl.org/downloads/2021.04.17/youtube-dl.exe", "youtubedl.exe");
            }
        }

        // 添加命令參數
        private void addArgs(string newArgs, bool needAdd)
        {
            if (needAdd)
            {
                argsTextBox.Text += newArgs;
            } 
            else
            {
                argsTextBox.Text = argsTextBox.Text.Replace(newArgs, "");
            }
        }

        private void disableSingleVideoRelate()
        {
            epsText.Text = "";
            epsText.IsEnabled = false;

            singleVideoCheckBox.IsEnabled = false;

            string args = "--no-playlist ";
            addArgs(args, false);
        }

        private void disableDownloadAllRelate()
        {
            epsText.Text = "";
            epsText.IsEnabled = false;

            downloadAllCheckBox.IsEnabled = false;

            string args = "--yes-playlist ";
            addArgs(args, false);
        }

        private void UrlText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
