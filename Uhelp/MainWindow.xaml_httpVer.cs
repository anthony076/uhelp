﻿using System;
using System.Windows;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace Uhelp
{

    public class Status
    {
        public string msg { get; set; }
        public string download_count { get; set; }
        
        public object[] list { get; set; }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        // ===================
        // urlText : input url
        // ===================
        private void urlText_GotFocus(object sender, RoutedEventArgs e)
        {
            if (urlText.Text == "https://www.youtube.com/watch?v=q_Mddckb7Lg&list=RDCMUC7wvKWegeU9FGzEcVhJVoDw&start_radio=1")
                urlText.Text = "";
        }

        private void urlText_LostFocus(object sender, RoutedEventArgs e)
        {
            if (urlText.Text == "")
            {
                urlText.Text = "https://www.youtube.com/watch?v=q_Mddckb7Lg&list=RDCMUC7wvKWegeU9FGzEcVhJVoDw&start_radio=1";
            }

        }

        // ===================
        // epsText : select move
        // ===================

        private void epsText_GotFocus(object sender, RoutedEventArgs e)
        {
            if (epsText.Text == "1-3,4,5,6")
                epsText.Text = "";
        }

        private void epsText_LostFocus(object sender, RoutedEventArgs e)
        {
            if (epsText.Text == "")
            {
                epsText.Text = "1-3,4,5,6";
            }

        }

        // ===================
        // buttons
        // ===================

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("btn clicked");

            string baseUrl = "http://127.0.0.1:7788/dl/";
            string param = String.Format("p={0}&u={1}" ,epsText.Text, urlText.Text);
            string b64Param = strTOb64(param);

            string dlUrl = baseUrl + b64Param;
            showMsg(dlUrl);


            string result = sendRequest(dlUrl);
            showMsg(result);
        }

        private void checkBtn_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("checking download status");

            string checkUrl = "http://127.0.0.1:7788/status";
            string result = sendRequest(checkUrl);

            Status curStatus = JsonConvert.DeserializeObject<Status>(result);

            showMsg(String.Format("{0} download is ongoing", curStatus.download_count));
        }

        // ===================
        // help functions
        // ===================

        private string sendRequest(string url)
        {

            // 定義請求
            //WebRequest request = WebRequest.Create("http://7pupu.changeip.co/");
            WebRequest request = WebRequest.Create(url);

            request.Method = "GET";

            // 送出請求
            using (var resp = (HttpWebResponse)request.GetResponse())

            // 讀取結果
            using (var streamReader = new StreamReader(resp.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return result;
            }
        }

        private void showMsg(string newMsg)
        {
            if (msg.Text != "")
            {
                msg.Text = "";
            }

            msg.Text = newMsg;
        }

        private string strTOb64(string str)
        {
            Byte[] bytesEncode = System.Text.Encoding.UTF8.GetBytes(str);
            string b64Param = Convert.ToBase64String(bytesEncode);

            return b64Param;
        }

        private string b64TOstr(string b64)
        {
            Byte[] bytesDecode = Convert.FromBase64String(b64);
            string resultText = System.Text.Encoding.UTF8.GetString(bytesDecode);

            return resultText;
        }
    }
}
