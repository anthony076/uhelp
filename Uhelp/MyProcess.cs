﻿using System;

using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace Uhelp
{
    public class MyProcess
    {
        public Process p;
        public MainWindow mw;

        public MyProcess(MainWindow mainwindow)
        {
            mw = mainwindow;
        }

        // =================
        // Initial
        // =================

        private void initProcess()
        {
            p = new Process();
            p.StartInfo.FileName = "cmd.exe";

            p.StartInfo.UseShellExecute = true;
            p.StartInfo.RedirectStandardInput = false;
            p.StartInfo.RedirectStandardOutput = false;
            p.StartInfo.RedirectStandardError = false;
            p.StartInfo.CreateNoWindow = false;

            // 啟動異步的事件觸發
            p.EnableRaisingEvents = true;
        }

        // =================
        // Hadlers
        // =================

        private void DonedHandler(object sender, EventArgs e)
        {
            TaskCompletionSource<bool> eventHandled;
            eventHandled = new TaskCompletionSource<bool>();
            eventHandled.TrySetResult(true);
        }

        public void openLinkHandler(object sender, RoutedEventArgs e)
        {
            var link = (Hyperlink) sender;
            var uri = link.NavigateUri.ToString();
            Process.Start(uri);
        }

        // =================
        // Execute
        // =================

        public void run(string args)
        {
            p.StartInfo.Arguments = args;
            p.Exited += new EventHandler(DonedHandler);
            p.Start();
        }

        // =================
        // Custom Tasks
        // =================

        public async Task<bool> syncCheckMP4()
        {
            Process proc = new Process();

            proc.StartInfo.FileName = "youtubedl.exe";

            string url = mw.urlText.Text.Split('&')[0];

            proc.StartInfo.Arguments = string.Format("-F {0}", url);
            proc.StartInfo.RedirectStandardOutput = true;

            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;

            proc.Start();

            string result = proc.StandardOutput.ReadToEnd();

            proc.Close();

            await Task.Delay(1);

            return result.Contains("mp4");
        }

        public async Task download(string args)
        {
            initProcess();

            bool haveMp4 = await syncCheckMP4();

            // 保持視窗開啟
            args = args.Insert(0, "/k ");

            // 加入影片格式參數
            if (haveMp4)
            {
                args += string.Format("-f mp4 ");
            }

            // 加入下載網址
            args += string.Format("\"{0}\"", mw.urlText.Text);

            Console.WriteLine("args = " + args);
            run(args);

            // 無作用，純粹避免 ide 拋出 沒有使用 await 的提示
            await Task.Delay(1);
        }
    }
}
